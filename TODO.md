# ToDo

## Общее

 + [ ] Добавить поддержку CSS.
   + [X] Сделать транслятор CSS в JSON. Пример:
     CSS:
     ```css
     Button {
       color: (255, 0, 0);
     }
     ```

     JSON:

     ```json
     {
       "Button":
         {"value":
           {
             "color": [255, 0, 0]
           }
         }
     }
     ```

 + [ ] Добавить класс `BaseController` и сделать его родителем классов `App` и `GameGUIController`
 + [X] Добавить папки `images`.
 + [X] Сделать окно изменяемым по размеру.

## Виджеты

 + [X] Добавить параметр `name` во все виджеты.
 + [X] Добавить параметр `font` во все виджеты.
 + [X] Изменить дефолтный шрифт во всех виджетах.
 + [X] Исправить баг с изменением числа в `NumericUpDown`

## Примеры
 + [X] Изменить пример `calc.py`
